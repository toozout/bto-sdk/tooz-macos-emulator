import SwiftUI
import tooz_ios_emulator_core

struct GlassView: View {
    
    @EnvironmentObject var tooz: ToozObservable
    @Binding var showGlassView: Bool
    
    var body: some View {
            VStack {
                Image("glasses_zoomed_in")
                    .resizable()
                    .aspectRatio(contentMode: .fit)
                    .overlay(
                        Image(nsImage: NSImage(data: tooz.currentFrame ?? Data())?.makeBlackTransparent() ?? NSImage(size: NSSize(width: 400, height: 640)))
                            .resizable()
                            .offset(x: 100)
                            .opacity(1.0)
                            .frame(width: 140, height: 227.5, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    )
                HStack {
                    VStack(alignment: .center, spacing: 4) {
                        ButtonView(text: "Short Tap", color: Color(Constants.colorGreyish), action: {
                            tooz.toozBluetooth?.sendButtonMessage(code: ButtonCode.a_1s)
                        })
                        ButtonView(text: "Long Tap", color: Color(Constants.colorGreyish), action: {
                            tooz.toozBluetooth?.sendButtonMessage(code: ButtonCode.a_1l)
                        })
                    }
                    VStack(alignment: .center, spacing: 4) {
                        ButtonView(text: "Forwad Swipe", color: Color(Constants.colorGreyish), action: {
                            tooz.toozBluetooth?.sendButtonMessage(code: ButtonCode.b_1s)
                        })
                        ButtonView(text: "Backward Swipe", color: Color(Constants.colorGreyish), action: {
                            tooz.toozBluetooth?.sendButtonMessage(code: ButtonCode.b_1l)
                        })
                    }
                }
                .padding(.top, 20)
            }
            .frame(width: Constants.appWidth, height: Constants.appHeight, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
            .overlay(Button(action: { self.showGlassView = false }, label: {
                Text("Help").foregroundColor(Color(Constants.colorGreyish))
            }).offset(x: -(/*@START_MENU_TOKEN@*/10.0/*@END_MENU_TOKEN@*/), y: -(10.0)), alignment: .bottomTrailing)
            .background(Image(nsImage: NSImage.init(imageLiteralResourceName: "background")))
    }
}

struct ButtonView: View {
        
    var text: String
    var color: Color
    var action: () -> Void = {}
    
    @GestureState private var isTapped = false
    
    var body: some View {
        HStack {
            ZStack {
                RoundedRectangle(cornerRadius: 8)
                    .frame(width: 150, height: 45, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
                    .foregroundColor(color)
                Text(text).bold().foregroundColor(.white)
            }
            .scaleEffect(isTapped ? Constants.tappedViewScale : 1.0)
            .gesture(
                DragGesture(minimumDistance: 0)
                    .updating($isTapped) { (_, isTapped, _) in
                        if !isTapped {
                            action()
                        }
                        isTapped = true
                    }
            )
        }
        
    }
}
