#import "ImageConverter.h"
#import <CoreImage/CoreImage.h>

@implementation NSImage (ColorTools)

-(NSImage*) makeBlackTransparent {
    
    NSColor *transparent = [NSColor.clearColor colorUsingColorSpace:[NSColorSpace sRGBColorSpace]];
    NSColor *black = [NSColor.blackColor colorUsingColorSpace:[NSColorSpace sRGBColorSpace]];
    
    CIImage *inputImage = [[CIImage alloc] initWithData:[self TIFFRepresentation]];
    
    const unsigned int size = 64;
    
    size_t cubeSize = size * size * size * sizeof ( float ) * 4;
    float *cube = (float *) malloc ( cubeSize );
    float rgb[3];
    
    size_t offset = 0;
    for (int z = 0; z < size; z++)
    {
        rgb[2] = ((double) z) / size;
        for (int y = 0; y < size; y++)
        {
            rgb[1] = ((double) y) / size;
            for (int x = 0; x < size; x++)
            {
                rgb[0] = ((double) x) / size;
                
                if ([self shouldClearColor:rgb withColor:black threshold:0.1]) {
                    cube[offset] = transparent.redComponent;
                    cube[offset + 1] = transparent.greenComponent;
                    cube[offset + 2] = transparent.blueComponent;
                    cube[offset + 3] = transparent.alphaComponent;
                } else {
                    cube[offset] = rgb[0];
                    cube[offset + 1] = rgb[1];
                    cube[offset + 2] = rgb[2];
                    cube[offset + 3] = 1.0;
                }
                
                offset += 4;
            }
        }
    }
    
    NSData *data = [NSData dataWithBytesNoCopy:cube length:cubeSize freeWhenDone:YES];
    CIFilter *colorCube = [CIFilter filterWithName:@"CIColorCube"];
    [colorCube setValue:[NSNumber numberWithInt:size] forKey:@"inputCubeDimension"];
    [colorCube setValue:data forKey:@"inputCubeData"];
    [colorCube setValue:inputImage forKey:kCIInputImageKey];
    CIImage *outputImage = [colorCube outputImage];
    
    NSImage *resultImage = [[NSImage alloc] initWithSize:[outputImage extent].size];
    NSCIImageRep *rep = [NSCIImageRep imageRepWithCIImage:outputImage];
    [resultImage addRepresentation:rep];
    
    return resultImage;
}

- (BOOL) shouldClearColor:(float[3])color1 withColor:(NSColor *)color2 threshold:(float)threshold {
    return ((fabs(color1[0] - color2.redComponent) < threshold) && (fabs(color1[1] - color2.greenComponent) < threshold) && (fabs(color1[2] - color2.blueComponent) < threshold));
}

@end
