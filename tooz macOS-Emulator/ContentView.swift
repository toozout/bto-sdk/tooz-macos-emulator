import SwiftUI
import tooz_ios_emulator_core

struct ContentView: View {
    
    @EnvironmentObject var tooz: ToozObservable
    @State private var showGlassView = true
    
    var body: some View {
        if showGlassView {
            GlassView(showGlassView: $showGlassView)
                .transition(AnyTransition.opacity.animation(.easeInOut(duration: Constants.animationDuration)))
                .environmentObject(tooz)
        } else {
            SettingsView(showGlassView: $showGlassView)
                .transition(AnyTransition.opacity.animation(.easeInOut(duration: Constants.animationDuration)))
                .environmentObject(tooz)
        }
    }
}
