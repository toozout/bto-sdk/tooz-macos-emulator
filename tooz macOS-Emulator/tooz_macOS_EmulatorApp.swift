import SwiftUI
import tooz_ios_emulator_core

@main
struct tooz_macOS_EmulatorApp: App {
        
    @NSApplicationDelegateAdaptor(AppDelegate.self) var delegate

    let tooz = ToozObservable()
    
    var body: some Scene {
        WindowGroup {
            ContentView().environmentObject(tooz)
        }
        
    }
    

}

class ToozObservable: ObservableObject, ConnectionStateListener, FrameReceivedListener {
    
    @Published var toozBluetooth: ToozBluetooth?
    @Published var currentFrame: Data?
    
    init() {
        initToozBluetooth()
    }
    
    func onConnectionStateChanged(connectionState: ConnectionState) {
        print("onConnectionStateChanged \(connectionState)")
        
        if case ConnectionState.disconnected = connectionState {
            currentFrame = nil
        }
        
    }
    
    func frameReceived(frame: Data) {
        currentFrame = frame
    }
    
    func resetBluetooth() {
        currentFrame = nil
        toozBluetooth?.disconnect()
        initToozBluetooth()
    }
    
    private func initToozBluetooth() {
        toozBluetooth = nil
        toozBluetooth = ToozBluetooth(connectionStateListener: self, frameListener: self, sensorRequestedListener: nil)
    }
        
}

class AppDelegate: NSObject, NSApplicationDelegate {
  
    func applicationDidFinishLaunching(_ notification: Notification) {
        print("Lifecycle::: applicationDidFinishLaunching")
    }
    
    func applicationDidHide(_ notification: Notification) {
        print("Lifecycle::: applicationDidHide")
    }
    
    func applicationWillHide(_ notification: Notification) {
        print("Lifecycle::: applicationWillHide")
    }
    
    func applicationWillUnhide(_ notification: Notification) {
        print("Lifecycle::: applicationWillUnhide")
    }
    
    func applicationWillBecomeActive(_ notification: Notification) {
        print("Lifecycle::: applicationWillBecomeActive")
    }
    
    func applicationWillResignActive(_ notification: Notification) {
        print("Lifecycle::: applicationWillResignActive")
    }
    
    func applicationDidBecomeActive(_ notification: Notification) {
        print("Lifecycle::: applicationDidBecomeActive")
    }
    
    func applicationDidResignActive(_ notification: Notification) {
        print("Lifecycle::: applicationDidResignActive")
    }
    
}


struct Constants {
    static let buttonWidth: CGFloat = 200
    static let buttonHeight: CGFloat = 30
    static let tappedViewScale: CGFloat = 1.05
    static let colorGreyish = "greyish"
    static let colorAlphaGrey = "alpha_grey"
    static let appWidth: CGFloat = 750
    static let appHeight: CGFloat = 550
    static let animationDuration: Double = 0.3
}

extension Data {
    public func printBytes() {
        print("START: \(self.map { String(format: " byte: %02x", $0) }.joined())")
    }
}

func printDebug(_ printValue: String) {
    #if DEBUG
    print(printValue)
    #endif
}
