//
//  tooz_ios_emulator_core.h
//  tooz-ios-emulator-core
//
//  Created by Daniel Sahm on 02.12.20.
//

#import <Foundation/Foundation.h>

//! Project version number for tooz_ios_emulator_core.
FOUNDATION_EXPORT double tooz_ios_emulator_coreVersionNumber;

//! Project version string for tooz_ios_emulator_core.
FOUNDATION_EXPORT const unsigned char tooz_ios_emulator_coreVersionString[];

// In this header, you should import all the public headers of your framework using statements like #import <tooz_ios_emulator_core/PublicHeader.h>


