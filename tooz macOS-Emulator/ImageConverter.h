#import <Cocoa/Cocoa.h>

@interface NSImage (ColorTools)

-(NSImage*) makeBlackTransparent;

@end
