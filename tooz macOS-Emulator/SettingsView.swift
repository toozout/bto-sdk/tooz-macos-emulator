import SwiftUI


struct SettingsView: View {
    
    @Environment(\.colorScheme) var colorScheme
    @EnvironmentObject var tooz: ToozObservable
    @Binding var showGlassView: Bool
    
    var body: some View {
        VStack {
            Text("Basic information about this emulator").bold().font(.title2).foregroundColor(.white)
            Text("This emulator emulates the basic functionality of the tooz Glasses. It displays image frames and sends button events to the client app. Note that it does not support sensor data like acceleration etc.").padding(EdgeInsets.init(top: 8, leading: 48, bottom: 8, trailing: 48)).foregroundColor(.white)
            Text("Design information").bold().font(.headline).foregroundColor(.white)
            Text("The glasses transform black colors in images to transparent. Therefore it is recommended to use a black background for views.").padding(EdgeInsets.init(top: 8, leading: 48, bottom: 2, trailing: 48)).foregroundColor(.white)
            Text("Limitations").bold().font(.headline).foregroundColor(.white)
            Text("1.) This emulator is slower than the physical glasses. This means that it takes longer to send frames to it than to the physical glasses. If you send frames over a longer period of time, this emulator will begin to lag.").padding(EdgeInsets.init(top: 8, leading: 48, bottom: 2, trailing: 48)).foregroundColor(.white)
            Text("2.) This emulator does not behave like the physical glasses in its underlying bluetooth implementation. This can lead to different behaviour, especially with disconnection events. Do not rely on the behaviour of this emulator when testing an app that is about to be released for production. Production tests must be performed on the physical glasses.").padding(EdgeInsets.init(top: 2, leading: 48, bottom: 2, trailing: 48)).foregroundColor(.white)
            Text("When encountering a bluetooth connectivity issue, please reset the emulator connection to the client device and restart the app on the client device.").padding(EdgeInsets.init(top: 8, leading: 48, bottom: 8, trailing: 48)).foregroundColor(.white)
            ButtonView(text: "Reset", color: Color(Constants.colorAlphaGrey), action: {
                tooz.resetBluetooth()
            })
        }
        .frame(width: Constants.appWidth, height: Constants.appHeight, alignment: /*@START_MENU_TOKEN@*/.center/*@END_MENU_TOKEN@*/)
        .background(Color(Constants.colorGreyish))
        .overlay(Button(action: { self.showGlassView = true }, label: {
            Text("Back").foregroundColor(colorScheme == .light ? .black : .white)
        }).offset(x: -(/*@START_MENU_TOKEN@*/10.0/*@END_MENU_TOKEN@*/), y: -(10.0)), alignment: .bottomTrailing)
    }
}
