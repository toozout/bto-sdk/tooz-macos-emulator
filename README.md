# tooz macOS-Emulator Beta-Version

## 1. Introduction

The tooz macOS-Emulator emulates the main functionalities of the tooz glasses:

* **Sending images**
* **Receiving button events**

## 2. Getting started

Clone this repository, open the project with Xcode and build the app. **Note**: The minimum requirement for this app is version 11 of macOS.

## 3. Troubleshooting

If any problem should arise, feel free to contact us at <dev@tooztech.com>. 

**Note**: The emulator does not emulate all the functionality of the tooz glasses and **it is much slower** - images take longer to be received by the emulator than by the physical glasses. Therefore it is strongly advised to not release an app to production that was not tested with the physcial glasses.

